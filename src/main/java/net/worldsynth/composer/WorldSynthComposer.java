/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer;

import javafx.stage.Stage;
import net.worldsynth.WorldSynth;
import net.worldsynth.composer.brush.BrushRegistry;
import net.worldsynth.composer.tool.ToolRegistry;
import net.worldsynth.composer.ui.WorldSynthComposerEditor;
import net.worldsynth.composition.Composition;
import net.worldsynth.composition.layer.Layer;

public class WorldSynthComposer {
	private static WorldSynthComposer instance;
	
	private WorldSynthComposerEditor composerEditor;
	
	public WorldSynthComposer() {
		if(instance != null) throw new IllegalStateException("Composer is already initialized");
		instance = this;
		
		new BrushRegistry(WorldSynth.getDirectoryConfig().getDirectory("brushes"));
		new ToolRegistry();
	}
	
	public static void openInComposer(Composition composition) {
		instance.composerEditor.openComposition(composition, null);
	}
	
	public static void openInComposer(Composition composition, Layer layer) {
		if(instance.composerEditor == null) {
			Stage composerStage = new Stage();
			composerStage.setOnCloseRequest(e -> {
				instance.composerEditor = null;
			});
			instance.composerEditor = new WorldSynthComposerEditor(composerStage);
		}
		
		if(layer == null) {
			instance.composerEditor.openComposition(composition);
		}
		else {
			instance.composerEditor.openComposition(composition, layer);
		}
		
		instance.composerEditor.focus();
	}
}
