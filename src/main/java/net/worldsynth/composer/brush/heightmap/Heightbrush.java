/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.brush.heightmap;

import net.worldsynth.composer.brush.Brush;

public abstract class Heightbrush extends Brush {
	
	public Heightbrush(String name) {
		super(name);
	}

	public abstract float[][] getBrushValues();
	
	public float getLocalHeight(int x, int z) {
		return getBrushValues()[x][z];
	}
	
	public float getLocalLerpHeight(double x, double z) {
		int x1 = (int) x;
		int x2 = x1+1;
		x2 = Math.min(x2, getBrushWidth()-1);
		int z1 = (int) z;
		int z2 = z1+1;
		z2 = Math.min(z2, getBrushHeight()-1);
		
		double u = x - Math.floor(x);
		double v = z - Math.floor(z);
		
		double val = (1.0-v)*(1.0-u)*getBrushValues()[x1][z1] + (1.0-v)*u*getBrushValues()[x2][z1] + v*(1.0-u)*getBrushValues()[x1][z2] + v*u*getBrushValues()[x2][z2];
		
		return (float) val;
	}
	
	public boolean isLocalContained(int x, int z) {
		if(x < 0.0 || x >= getBrushWidth() || z < 0.0 || z >= getBrushHeight()) {
			return false;
		}
		return true;
	}
}
