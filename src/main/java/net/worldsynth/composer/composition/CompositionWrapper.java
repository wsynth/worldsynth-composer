/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.composition;

import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import net.worldsynth.composer.layer.LayerWrapper;
import net.worldsynth.composer.layer.heightmap.LayerWrapperHeightmap;
import net.worldsynth.composition.Composition;
import net.worldsynth.composition.layer.Layer;
import net.worldsynth.composition.layer.heightmap.LayerHeightmap;

public class CompositionWrapper {
	
	private final Composition composition;

	private final ObservableList<LayerWrapper<?>> compositionLayers = FXCollections.observableArrayList();
	private final SimpleObjectProperty<LayerWrapper<?>> selectedLayer = new SimpleObjectProperty<LayerWrapper<?>>(null);
	
	public CompositionWrapper(Composition composition) {
		this.composition = composition;
		for(Layer layer: composition.getLayersList()) {
			compositionLayers.add(wrapLayer(layer));
		}
	}
	
	private LayerWrapper<?> wrapLayer(Layer layer) {
		if(layer instanceof LayerHeightmap) {
			return new LayerWrapperHeightmap((LayerHeightmap) layer);
		}
		throw new IllegalArgumentException("Layer type has no layer wrapper");
	}
	
	public Composition getWrappedComposition() {
		return composition;
	}
	
	public ObservableList<LayerWrapper<?>> getObservableLayersList() {
		return compositionLayers;
	}
	
	public SimpleObjectProperty<LayerWrapper<?>> selectedLayerProperty() {
		return selectedLayer;
	}
	
	public LayerWrapper<?> getSelectedLayer() {
		return selectedLayer.get();
	}
	
	public void setSelectedLayer(LayerWrapper<?> selectedLayer) {
		this.selectedLayer.set(selectedLayer);
	}
	
	public void setSelectedLayer(Layer layer) {
		for(LayerWrapper<?> lw: compositionLayers) {
			if(lw.getLayer() == layer) {
				selectedLayer.set(lw);
				return;
			}
		}
	}
}
