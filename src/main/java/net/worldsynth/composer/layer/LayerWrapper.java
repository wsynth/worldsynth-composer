/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import net.worldsynth.composition.layer.Layer;

public abstract class LayerWrapper<T extends Layer> {
	
	private final Layer layer;
	private final LayerRender<T> render;
	
	private final SimpleStringProperty propertyLayerName;
	private final SimpleObjectProperty<BlendMode> propertyLayerPreviewBlendMode;
	private final SimpleObjectProperty<Image> propertyLayerTumbnail;
	private final SimpleBooleanProperty propertyLayerLocked;
	private final SimpleBooleanProperty propertyLayerActive;
	
	
	public LayerWrapper(T layer, LayerRender<T> render) {
		this.layer = layer;
		this.render = render;
		
		propertyLayerName = new SimpleStringProperty(layer.getName());
		propertyLayerName.addListener((observable, oldValue, newValue) -> {
			layer.setName(newValue);
		});
		
		propertyLayerPreviewBlendMode = new SimpleObjectProperty<BlendMode>(layer.getPreviewBlendMode());
		propertyLayerPreviewBlendMode.addListener((observable, oldValue, newValue) -> {
			layer.setPreviewBlendMode(newValue);
		});
		
		propertyLayerLocked = new SimpleBooleanProperty(layer.getLocked());
		propertyLayerLocked.addListener((observable, oldValue, newValue) -> {
			layer.setLocked(newValue);
		});
		
		propertyLayerActive = new SimpleBooleanProperty(layer.getActive());
		propertyLayerActive.addListener((observable, oldValue, newValue) -> {
			layer.setActive(newValue);
		});
		
		propertyLayerTumbnail = new SimpleObjectProperty<Image>(getDefaultLayerTumbnail());
	}
	
	public Layer getLayer() {
		return layer;
	}
	
	public LayerRender<T> getLayerRender() {
		return render;
	}
	
	public SimpleStringProperty layerNameProperty() {
		return propertyLayerName;
	}
	
	public void setLayerName(String layerName) {
		layerNameProperty().set(layerName);
	}
	public String getLayerName() {
		return layerNameProperty().get();
	}

	public SimpleObjectProperty<BlendMode> layerPreviewBlendmodeProperty() {
		return propertyLayerPreviewBlendMode;
	}
	
	public void setLayerPreviewBlemndMode(BlendMode blendMode) {
		layerPreviewBlendmodeProperty().set(blendMode);
	}
	public BlendMode getLayerPreviewBlendMode() {
		return layerPreviewBlendmodeProperty().get();
	}

	public SimpleObjectProperty<Image> layerTumbnailProperty() {
		return propertyLayerTumbnail;
	}
	
	public void setLayerTumbnail(Image layerTumbnail) {
		layerTumbnailProperty().set(layerTumbnail);
	}
	public Image getLayerTumbnail() {
		return layerTumbnailProperty().get();
	}

	public SimpleBooleanProperty layerActiveProperty() {
		return propertyLayerActive;
	}
	
	public void setLayerActive(boolean layerActive) {
		layerActiveProperty().set(layerActive);;
	}
	public boolean isLayerActive() {
		return layerActiveProperty().get();
	}

	public SimpleBooleanProperty layerLockedProperty() {
		return propertyLayerLocked;
	}
	
	public void setLayerLocked(boolean layerLocked) {
		layerLockedProperty().set(layerLocked);
	}
	public boolean isLayerLocked() {
		return layerLockedProperty().get();
	}
	
	public abstract Image getDefaultLayerTumbnail();
}
