/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer;

import javafx.scene.image.Image;
import net.worldsynth.composition.layer.LayerNull;

public class LayerWrapperNull extends LayerWrapper<LayerNull> {

	public LayerWrapperNull(LayerNull layer) {
		super(layer, null);
	}

	@Override
	public Image getDefaultLayerTumbnail() {
		return null;
	}

}
