/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer.colormap;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import javafx.beans.value.ObservableValue;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import net.worldsynth.composer.layer.LayerRender;
import net.worldsynth.composer.ui.navcanvas.Coordinate;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.composer.ui.navcanvas.Pixel;
import net.worldsynth.composition.layer.colormap.ColormapRegion;
import net.worldsynth.composition.layer.colormap.LayerColormap;

public class LayerRenderColormap extends LayerRender<LayerColormap> {
	
	private RenderStyle renderStyle = RenderStyle.COLOR;
	
	private final HashMap<ColormapRegion, Image> regionRenders = new HashMap<ColormapRegion, Image>();
	
	public LayerRenderColormap(LayerColormap layer) {
		super(layer);
	}

	@Override
	protected void updateRender(LayerColormap layer, GraphicsContext g, NavigationalCanvas navCanvas) {
		g.clearRect(0, 0, 10000, 10000);
		
		Iterator<Entry<String, ColormapRegion>> i = layer.regionmap.entrySet().iterator();
		while(i.hasNext()) {
			ColormapRegion region = i.next().getValue();
			if(region.regionRebuildRenderIsQued()) {
				regionRenders.put(region, buildRegionRender(layer, region));
				region.edqueRebuildRender();
			}
			drawRegion(region, regionRenders.get(region), g, navCanvas);
		}
	}
	
	@Override
	protected Pane renderParameterPane() {
		GridPane renderParameterPane = new GridPane();
		
		renderParameterPane.add(new Label("Style: "), 0, 0);
		ComboBox<RenderStyle> renderStyleComboBox = new ComboBox<RenderStyle>();
		for(RenderStyle style: RenderStyle.values()) {
			renderStyleComboBox.getItems().add(style);
		}
		renderParameterPane.add(renderStyleComboBox, 1, 0);
		
		renderStyleComboBox.getSelectionModel().select(RenderStyle.COLOR);
		renderStyleComboBox.valueProperty().addListener((ObservableValue<? extends RenderStyle> observable, RenderStyle oldValue, RenderStyle newValue) -> {
			renderStyle = newValue;
		});
		
		return renderParameterPane;
	}
	
	private void drawRegion(ColormapRegion region, Image regionimage, GraphicsContext g, NavigationalCanvas navCanvas) {
		Pixel corner = new Pixel(new Coordinate(region.getRegionX()*256, region.getRegionZ()*256), navCanvas);
		g.drawImage(regionimage, corner.x, corner.y, 256.0*navCanvas.getZoom(), 256.0*navCanvas.getZoom());
	}
	
	private Image buildRegionRender(LayerColormap layer, ColormapRegion region) {
		WritableImage regionRenderImage = new WritableImage(256, 256);
		PixelWriter pw = regionRenderImage.getPixelWriter();
		
		if(renderStyle == RenderStyle.COLOR) {
			for(int u = 0; u < 256; u++) {
				for(int v = 0; v < 256; v++) {
					pw.setColor(u, v, Color.color(region.regionValues[u][v][0], region.regionValues[u][v][1], region.regionValues[u][v][2]));
				}
			}
		}
		
		return regionRenderImage;
	}
	
	private float[] getColorAtRegionLocal(LayerColormap layer, ColormapRegion region, int regionLocalX, int regionLocalZ) {
		if(regionLocalX < 0 || regionLocalX >= 256) {
			return layer.getColorAt(region.getRegionX()*256+regionLocalX, region.getRegionZ()*256+regionLocalZ);
		}
		else if(regionLocalZ < 0 || regionLocalZ >= 256) {
			return layer.getColorAt(region.getRegionX()*256+regionLocalX, region.getRegionZ()*256+regionLocalZ);
		}
		return region.regionValues[regionLocalX][regionLocalZ];
	}
	
	private enum RenderStyle {
		COLOR;
	}
}
