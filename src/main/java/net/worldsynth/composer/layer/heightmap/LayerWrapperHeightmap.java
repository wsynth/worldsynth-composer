/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.layer.heightmap;

import javafx.scene.image.Image;
import net.worldsynth.composer.layer.LayerWrapper;
import net.worldsynth.composition.layer.heightmap.LayerHeightmap;

public class LayerWrapperHeightmap extends LayerWrapper<LayerHeightmap> {
	
	public LayerWrapperHeightmap(LayerHeightmap layer) {
		super(layer, new LayerRenderHeightmap(layer));
	}
	
	@Override
	public Image getDefaultLayerTumbnail() {
		return new Image(getClass().getClassLoader().getResourceAsStream("icons/layers/heightmap.png"));
	}
}
