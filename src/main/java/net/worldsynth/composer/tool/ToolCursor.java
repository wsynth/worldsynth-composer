/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.composition.layer.Layer;

public class ToolCursor extends Tool<Layer> {
	
	@Override
	public boolean isApplicableToLayer(Layer layer) {
		return true;
	}
	
	@Override
	protected Image toolIconImage() {
		return new Image((getClass().getClassLoader().getResourceAsStream("icons/tools/cursor.png")));
	}

	@Override
	public Pane getToolParameterPane() {
		return new Pane();
	}

	@Override
	public void onPrimaryPressed(double x, double z, double pressure, Layer layer) {
	}

	@Override
	public void onSecondaryPressed(double x, double z, double pressure, Layer layer) {
	}

	@Override
	public void onPrimaryDown(double x, double z, double pressure, Layer layer) {
	}

	@Override
	public void onSecondaryDown(double x, double z, double pressure, Layer layer) {
	}

	@Override
	public void onPrimaryDragged(double x, double z, double pressure, Layer layar) {
	}

	@Override
	public void onSecondaryDragged(double x, double z, double pressure, Layer layar) {
	}

	@Override
	public void onPrimaryReleased(double x, double z, double pressure, Layer layar) {
	}

	@Override
	public void onSecondaryReleased(double x, double z, double pressure, Layer layar) {
	}

	@Override
	public void renderTool(double x, double z, GraphicsContext g, NavigationalCanvas navCanvas) {
	}
}
