/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.tool;

import java.util.ArrayList;

import com.sun.istack.internal.localization.NullLocalizable;

import net.worldsynth.composer.layer.LayerWrapper;
import net.worldsynth.composer.layer.LayerWrapperNull;
import net.worldsynth.composer.tool.heightmap.ToolHeightmapElevation;
import net.worldsynth.composer.tool.heightmap.ToolHeightmapFlatten;
import net.worldsynth.composer.tool.heightmap.ToolHeightmapSmoothen;
import net.worldsynth.composition.layer.LayerNull;

public class ToolRegistry {
	
	public static ArrayList<Tool<?>> REGISTER = new ArrayList<Tool<?>>();
	
	public ToolRegistry() {
		registerTools();
	}
	
	private void registerTools() {
		//Null tools
		REGISTER.add(new ToolCursor());
		
		//Heightmap tools
		REGISTER.add(new ToolHeightmapElevation());
		REGISTER.add(new ToolHeightmapFlatten());
		REGISTER.add(new ToolHeightmapSmoothen());
	}
	
	public static ArrayList<Tool<?>> getToolsForLayer(LayerWrapper<?> layer) {
		if(layer == null) {
			layer = new LayerWrapperNull(new LayerNull("null"));
		}
		
		ArrayList<Tool<?>> layerTools = new ArrayList<Tool<?>>();
		for(Tool<?> t: REGISTER) {
			if(t.isApplicableToLayer(layer.getLayer())) {
				layerTools.add(t);
			}
		}
		
		return layerTools;
	}
}
