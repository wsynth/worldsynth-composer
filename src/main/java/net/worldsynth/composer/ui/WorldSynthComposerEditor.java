/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.ui;

import java.util.HashMap;

import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.worldsynth.composer.composition.CompositionWrapper;
import net.worldsynth.composer.ui.composition.CompositionTab;
import net.worldsynth.composer.ui.toolbar.ToolBar;
import net.worldsynth.composition.Composition;
import net.worldsynth.composition.layer.Layer;

public class WorldSynthComposerEditor {
	
	//The main stage for Composer
	private final Stage stage;
	
	//Content
	private final MenuBar menuBar;
	private final TabPane compositionsTabPane = new TabPane();
	private final BorderPane layerBrowserPane = new BorderPane();
	
	public static final ToolBar toolbar = new ToolBar();
	public static final BorderPane toolParametersPaneWrapper = new BorderPane();
	
	//Compositions
	private final HashMap<Composition, CompositionTab> openCompositions = new HashMap<Composition, CompositionTab>();
	
	public WorldSynthComposerEditor(Stage stage) {
		this.stage = stage;
		stage.initStyle(StageStyle.DECORATED);
		stage.setTitle("WorldSynth Composer pre-alpha");
		stage.setMinWidth(800);
		stage.setMinHeight(700);
		Image stageIcon = new Image(getClass().getClassLoader().getResourceAsStream("worldSynthIcon.png"));
		stage.getIcons().add(stageIcon);
		
		//Build up main UI
		//Add menu bar at top
		BorderPane root = new BorderPane();
		menuBar = new MenuBar(new Menu("File"), new Menu("Edit"), new Menu("Help"));
		root.setTop(menuBar);
		
		//Add toolbar at left side
		root.setLeft(toolbar);
		
		//Add project tabs
		compositionsTabPane.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			CompositionTab compTab = (CompositionTab) newValue;
			layerBrowserPane.setCenter(compTab.getLayerBrowser());
			
			CompositionWrapper compWrapper = compTab.getCompositionWrapper();
			toolbar.setToolsForLayer(compWrapper.getSelectedLayer());
		});
		
		root.setCenter(compositionsTabPane);
		
		//Add right side content
		BorderPane parametersAndLayersPane = new BorderPane();
		parametersAndLayersPane.setTop(toolParametersPaneWrapper);
		parametersAndLayersPane.setCenter(layerBrowserPane);
		root.setRight(parametersAndLayersPane);
		
		//Setup the scene and style
		Scene scene = new Scene(root, 1500, 800);
		scene.getStylesheets().add(getClass().getClassLoader().getResource("DarkThemeComposer.css").toExternalForm());
		
		stage.setScene(scene);
		stage.show();
	}
	
	public void focus() {
		stage.setIconified(false);
		stage.requestFocus();
	}
	
	public void openComposition(Composition composition, Layer layer) {
		CompositionTab compTab = openComposition(composition);
		compTab.getCompositionWrapper().setSelectedLayer(layer);
	}
	
	public CompositionTab openComposition(Composition composition) {
		if(!openCompositions.containsKey(composition)) {
			CompositionWrapper compWrapper = new CompositionWrapper(composition);
			CompositionTab compTab = new CompositionTab(compWrapper);
			openCompositions.put(composition, compTab);
			compositionsTabPane.getTabs().add(compTab);
			compositionsTabPane.getSelectionModel().select(compTab);
		}
		
		CompositionTab compTab = openCompositions.get(composition);
		compositionsTabPane.getSelectionModel().select(compTab);
		return compTab;
	}
	
	public void closeComposition(Composition composition) {
		if(!openCompositions.containsKey(composition)) {
			return;
		}
		
		CompositionTab compTab = openCompositions.get(composition);
		compositionsTabPane.getTabs().remove(compTab);
	}
}
