/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.ui.brushbrowser;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import net.worldsynth.composer.brush.Brush;

public class BrushView extends StackPane {
	
	private final Brush brush;
	private final ImageView imageView;
	private final Rectangle highlightRect;
	
	public BrushView(Brush brush, BrushBrowser brushBrowser) {
		this.brush = brush;
		
		imageView = new ImageView();
		imageView.fitHeightProperty().bind(heightProperty());
        imageView.fitWidthProperty().bind(widthProperty());
        imageView.setPreserveRatio(true);
		imageView.setSmooth(true);
		setImage(brush.getBrushTumbnail());
		
		highlightRect = new Rectangle();
		highlightRect.setStroke(Color.TRANSPARENT);
		highlightRect.setFill(Color.TRANSPARENT);
		highlightRect.heightProperty().bind(heightProperty());
		highlightRect.widthProperty().bind(widthProperty());
		
		getChildren().addAll(imageView, highlightRect);
		
		setOnMouseClicked(e -> {
			brushBrowser.setBrushSelection(this);
		});
	}
	
	public Brush getBrush() {
		return brush;
	}
	
	public void setImage(Image brushTumbnail) {
		imageView.setImage(brushTumbnail);
	}
	
	public void setSelected(boolean selected) {
		if(selected) {
			highlightRect.setStroke(Color.WHITE);
			highlightRect.setFill(Color.color(0.6784314, 0.84705883, 0.9019608, 0.2));
		}
		else {
			highlightRect.setStroke(Color.TRANSPARENT);
			highlightRect.setFill(Color.TRANSPARENT);
		}
	}
}
