/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.ui.composition;

import javafx.scene.control.Tab;
import net.worldsynth.composer.composition.CompositionWrapper;
import net.worldsynth.composer.ui.WorldSynthComposerEditor;
import net.worldsynth.composer.ui.layerbrowser.LayerBrowser;

public class CompositionTab extends Tab {
	
	private final CompositionWrapper compositionWrapper;
	private final CompositionViewer compositionViewer;
	private final LayerBrowser layerBrowser;
	
	public CompositionTab(CompositionWrapper compositionWrapper) {
		this.compositionWrapper = compositionWrapper;
		setText(compositionWrapper.getWrappedComposition().getName());
		
		layerBrowser = new LayerBrowser(compositionWrapper);
		
		compositionViewer = new CompositionViewer(compositionWrapper);
		setContent(compositionViewer);
		
		compositionWrapper.selectedLayerProperty().addListener((observable, oldValue, newValue) -> {
			WorldSynthComposerEditor.toolbar.setToolsForLayer(newValue);
		});
	}
	
	public CompositionWrapper getCompositionWrapper() {
		return compositionWrapper;
	}
	
	public LayerBrowser getLayerBrowser() {
		return layerBrowser;
	}
}
