/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.ui.composition;

import java.util.HashMap;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.BlendMode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import net.worldsynth.composer.composition.CompositionWrapper;
import net.worldsynth.composer.layer.LayerWrapper;
import net.worldsynth.composer.tool.Tool;
import net.worldsynth.composer.ui.WorldSynthComposerEditor;
import net.worldsynth.composer.ui.navcanvas.Coordinate;
import net.worldsynth.composer.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.composer.ui.navcanvas.Pixel;

public class CompositionViewer extends BorderPane implements NavigationalCanvas {
	
	private final CompositionWrapper compositionWrapper;
	
	private final Canvas backgroundCanvas = new Canvas();
	private final Canvas foregroundCanvas = new Canvas();
	private final Canvas toolCanvas = new Canvas();
	private final Group canvasGroup = new Group();
	private final HashMap<LayerWrapper<?>, Canvas> canvasMap = new HashMap<LayerWrapper<?>, Canvas>();
	
	private double centerCoordX = 0.0;
	private double centerCoordY = 0.0;
	private double zoom = 1.0;
	
	private double lastMouseX = 0;
	private double lastMouseY = 0;
	
	private boolean primaryMouseButton = false;
	private boolean secondaryMouseButton = false;
	private boolean midleMouseButton = false;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public CompositionViewer(CompositionWrapper compositionWrapper) {
		this.compositionWrapper = compositionWrapper;
		
		getChildren().add(backgroundCanvas);
		getChildren().add(canvasGroup);
		getChildren().add(foregroundCanvas);
		getChildren().add(toolCanvas);
		toolCanvas.setBlendMode(BlendMode.ADD);
		
		Timeline periodicApply = new Timeline(new KeyFrame(Duration.seconds(0.05), new EventHandler<ActionEvent>() {
			@Override
		    public void handle(ActionEvent event) {
				if(compositionWrapper.getSelectedLayer().isLayerLocked()) return;
				
		    	double x = getCurrentMouseCoordinateX();
				double z = getCurrentMouseCoordinateZ();
				
				if(primaryMouseButton) {
					Tool tool = WorldSynthComposerEditor.toolbar.getSelectedTool();
					tool.onPrimaryDown(x, z, 1.0, tool.castLayer(compositionWrapper.getSelectedLayer()));
				}
				else if(secondaryMouseButton) {
					Tool tool = WorldSynthComposerEditor.toolbar.getSelectedTool();
					tool.onSecondaryDown(x, z, 1.0, tool.castLayer(compositionWrapper.getSelectedLayer()));
				}
				updatePreview();
		    }
		}));
		
		setOnMousePressed(e -> {
			if(e.isSynthesized()) return;
			else if(compositionWrapper.getSelectedLayer().isLayerLocked()) return;
			
			lastMouseX = e.getX();
			lastMouseY = e.getY();
			
			if(e.getButton() == MouseButton.PRIMARY) primaryMouseButton = true;
			else if(e.getButton() == MouseButton.SECONDARY) secondaryMouseButton = true;
			else if(e.getButton() == MouseButton.MIDDLE) midleMouseButton = true;
			
			double x = getCurrentMouseCoordinateX();
			double z = getCurrentMouseCoordinateZ();
			
			if(primaryMouseButton) {
				Tool tool = WorldSynthComposerEditor.toolbar.getSelectedTool();
				tool.onPrimaryPressed(x, z, 1.0, tool.castLayer(compositionWrapper.getSelectedLayer()));
				updatePreview();
				periodicApply.setCycleCount(Timeline.INDEFINITE);
				periodicApply.play();
			}
			else if(secondaryMouseButton) {
				Tool tool = WorldSynthComposerEditor.toolbar.getSelectedTool();
				tool.onSecondaryPressed(x, z, 1.0, tool.castLayer(compositionWrapper.getSelectedLayer()));
				updatePreview();
				periodicApply.setCycleCount(Timeline.INDEFINITE);
				periodicApply.play();
			}
		});
		
		setOnMouseDragged(e -> {
			if(e.isSynthesized()) return;
			else if(compositionWrapper.getSelectedLayer().isLayerLocked()) {
				lastMouseX = e.getX();
				lastMouseY = e.getY();
				
				toolCanvas.getGraphicsContext2D().clearRect(0, 0, 10000, 10000);
				Tool<?> tool = WorldSynthComposerEditor.toolbar.getSelectedTool();
				tool.renderTool(getCurrentMouseCoordinateX(), getCurrentMouseCoordinateZ(), toolCanvas.getGraphicsContext2D(), this);
				
				return;
			}
			
			double diffX = lastMouseX - e.getX();
			double diffY = lastMouseY - e.getY();
			lastMouseX = e.getX();
			lastMouseY = e.getY();
			
			double x = getCurrentMouseCoordinateX();
			double z = getCurrentMouseCoordinateZ();
			if(midleMouseButton) {
				centerCoordX += diffX / zoom;
				centerCoordY += diffY / zoom;
			}
			else if(primaryMouseButton) {
				Tool tool = WorldSynthComposerEditor.toolbar.getSelectedTool();
				tool.onPrimaryDragged(x, z, 1.0, tool.castLayer(compositionWrapper.getSelectedLayer()));
			}
			else if(secondaryMouseButton) {
				Tool tool = WorldSynthComposerEditor.toolbar.getSelectedTool();
				tool.onSecondaryDragged(x, z, 1.0, tool.castLayer(compositionWrapper.getSelectedLayer()));
			}
			
			updatePreview();
		});
		
		setOnMouseReleased(e -> {
			if(e.isSynthesized()) return;
			else if(compositionWrapper.getSelectedLayer().isLayerLocked()) return;
			
			lastMouseX = e.getX();
			lastMouseY = e.getY();
			
			double x = getCurrentMouseCoordinateX();
			double z = getCurrentMouseCoordinateZ();
			if(primaryMouseButton) {
				periodicApply.stop();
				Tool tool = WorldSynthComposerEditor.toolbar.getSelectedTool();
				tool.onPrimaryReleased(x, z, 1.0, tool.castLayer(compositionWrapper.getSelectedLayer()));
				updatePreview();
			}
			else if(secondaryMouseButton) {
				periodicApply.stop();
				Tool tool = WorldSynthComposerEditor.toolbar.getSelectedTool();
				tool.onSecondaryReleased(x, z, 1.0, tool.castLayer(compositionWrapper.getSelectedLayer()));
				updatePreview();
			}
			
			if(e.getButton() == MouseButton.PRIMARY) primaryMouseButton = false;
			else if(e.getButton() == MouseButton.SECONDARY) secondaryMouseButton = false;
			else if(e.getButton() == MouseButton.MIDDLE) midleMouseButton = false;
			
		});
		
		setOnTouchPressed(e -> {
			lastMouseX = e.getTouchPoint().getX();
			lastMouseY = e.getTouchPoint().getY();
		});
		
		setOnTouchMoved(e -> {
			double diffX = lastMouseX - e.getTouchPoint().getX();
			double diffY = lastMouseY - e.getTouchPoint().getY();
			lastMouseX = e.getTouchPoint().getX();
			lastMouseY = e.getTouchPoint().getY();
			
			centerCoordX += diffX / zoom;
			centerCoordY += diffY / zoom;
			
			updatePreview();
		});
		
		//TODO implement zoom
		setOnScroll(e -> {
			if(e.isDirect()) return;
			double maxZoom = 16.0;
			double minZoom = 0.2;
			
			double lastZoom = zoom;
			zoom += e.getDeltaY() / e.getMultiplierY() * zoom / 10;
			if(zoom < minZoom) zoom = minZoom;
			else if(zoom > maxZoom) zoom = maxZoom;
			
			if(lastZoom != zoom) {
				updatePreview();
			}
		});
		
		setOnMouseEntered(e -> {
			if(e.isSynthesized()) return;
			lastMouseX = e.getX();
			lastMouseY = e.getY();
		});
		
		setOnMouseMoved(e -> {
			if(e.isSynthesized()) return;
			lastMouseX = e.getX();
			lastMouseY = e.getY();
			
			toolCanvas.getGraphicsContext2D().clearRect(0, 0, 10000, 10000);
			Tool<?> tool = WorldSynthComposerEditor.toolbar.getSelectedTool();
			tool.renderTool(getCurrentMouseCoordinateX(), getCurrentMouseCoordinateZ(), toolCanvas.getGraphicsContext2D(), this);
		});
	}

	@Override
	protected final void layoutChildren() {
		super.layoutChildren();
		final double x = snappedLeftInset();
		final double y = snappedTopInset();
		// Java 9 - snapSize is depricated used snapSizeX() and snapSizeY() accordingly
		final double w = snapSize(getWidth()) - x - snappedRightInset();
		final double h = snapSize(getHeight()) - y - snappedBottomInset();

		backgroundCanvas.setLayoutX(x);
		backgroundCanvas.setLayoutY(y);
		backgroundCanvas.setWidth(w);
		backgroundCanvas.setHeight(h);

		foregroundCanvas.setLayoutX(x);
		foregroundCanvas.setLayoutY(y);
		foregroundCanvas.setWidth(w);
		foregroundCanvas.setHeight(h);
		
		toolCanvas.setLayoutX(x);
		toolCanvas.setLayoutY(y);
		toolCanvas.setWidth(w);
		toolCanvas.setHeight(h);

		canvasMap.entrySet().forEach(t -> {
			t.getValue().setLayoutX(x);
			t.getValue().setLayoutY(y);
			t.getValue().setWidth(w);
			t.getValue().setHeight(h);
		});

		updatePreview();
	}
	
	@Override
	public final double getCenterCoordinateX() {
		return centerCoordX;
	}

	@Override
	public final double getCenterCoordinateY() {
		return centerCoordY;
	}

	@Override
	public final double getZoom() {
		return zoom;
	}
	
	public final double getCurrentMouseCoordinateX() {
		return new Coordinate(new Pixel(lastMouseX, lastMouseY), this).x;
	}
	
	public final double getCurrentMouseCoordinateZ() {
		return new Coordinate(new Pixel(lastMouseX, lastMouseY), this).y;
	}
	
	public void updatePreview() {
		backgroundCanvas.getGraphicsContext2D().setFill(Color.BLACK);
		backgroundCanvas.getGraphicsContext2D().fillRect(0, 0, getWidth(), getHeight());
		
		boolean firstLayer = true;
		for(LayerWrapper<?> layer: compositionWrapper.getObservableLayersList()) {
			if(!canvasMap.containsKey(layer)) {
				Canvas newCanvas = new Canvas();
				canvasMap.put(layer, newCanvas);
				canvasGroup.getChildren().add(newCanvas);
				layoutChildren();
			}
			
			Canvas layerCanvas = canvasMap.get(layer);
			if(firstLayer && layer.isLayerActive()) {
				layerCanvas.setBlendMode(null);
				firstLayer = false;
			}
			else {
				layerCanvas.setBlendMode(layer.getLayerPreviewBlendMode());
			}
			layerCanvas.setOpacity(1);
			GraphicsContext g = layerCanvas.getGraphicsContext2D();
			if(layer.isLayerActive()) {
				layer.getLayerRender().updateRender(g, this);
			}
			else {
				layer.getLayerRender().clearRender(g, this);
			}
		}
		
		foregroundCanvas.getGraphicsContext2D().clearRect(0, 0, 10000, 10000);
		renderGrid(foregroundCanvas.getGraphicsContext2D());
		
		toolCanvas.getGraphicsContext2D().clearRect(0, 0, 10000, 10000);
		Tool<?> tool = WorldSynthComposerEditor.toolbar.getSelectedTool();
		tool.renderTool(getCurrentMouseCoordinateX(), getCurrentMouseCoordinateZ(), toolCanvas.getGraphicsContext2D(), this);
	}
	
	private void renderGrid(GraphicsContext g) {
		//Draw the grid
		g.setStroke(Color.web("#363636", Math.max(0.0, Math.min(0.5, getZoom()*2-0.5))));
		g.setLineWidth(1.0);
		float gridIncrement = 16;
		for(float cx = gridIncrement; (new Pixel(new Coordinate(cx, 0), this)).x < getWidth(); cx += gridIncrement) {
			double x = (new Pixel(new Coordinate(cx, 0), this)).x;
			g.strokeLine(x, 0, x, getHeight());
		}
		for(float cx = -gridIncrement; (new Pixel(new Coordinate(cx, 0), this)).x > 0; cx -= gridIncrement) {
			double x = (new Pixel(new Coordinate(cx, 0), this)).x;
			g.strokeLine(x, 0, x, getHeight());
		}
		for(float cy = gridIncrement; (new Pixel(new Coordinate(0, cy), this)).y < getHeight(); cy += gridIncrement) {
			double y = (new Pixel(new Coordinate(0, cy), this)).y;
			g.strokeLine(0, y, getWidth(), y);
		}
		for(float cy = -gridIncrement; (new Pixel(new Coordinate(0, cy), this)).y > 0; cy -= gridIncrement) {
			double y = (new Pixel(new Coordinate(0, cy), this)).y;
			g.strokeLine(0, y, getWidth(), y);
		}
		
		//Draw center cross;
		g.setStroke(Color.web("#000000", 0.2));
		g.setLineWidth(2.0);
		Pixel centerCoordinatePixel = new Pixel(new Coordinate(0, 0), this);
		g.strokeLine(centerCoordinatePixel.x, 0, centerCoordinatePixel.x, getHeight());
		g.strokeLine(0, centerCoordinatePixel.y, getWidth(), centerCoordinatePixel.y);
	}
}
