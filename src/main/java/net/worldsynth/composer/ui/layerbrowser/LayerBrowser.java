/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.ui.layerbrowser;

import java.util.HashMap;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlendMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import net.worldsynth.composer.composition.CompositionWrapper;
import net.worldsynth.composer.layer.LayerWrapper;

public class LayerBrowser extends BorderPane {
	
	private final CompositionWrapper compositionWrapper;
	
	private final VBox layersListVBox = new VBox();
	private final HashMap<LayerWrapper<?>, LayerView> layersMap = new HashMap<LayerWrapper<?>, LayerView>();
		
	public LayerBrowser(CompositionWrapper compositionWrapper) {
		this.compositionWrapper = compositionWrapper;
		
		layersListVBox.setPadding(new Insets(2.0));
		layersListVBox.setSpacing(2.0);
		layersListVBox.setFocusTraversable(false);
		setFocusTraversable(false);
		
		//Setup layer preview blending pane
		Label previewBlendLabel = new Label("Preview blend:");
		previewBlendLabel.setAlignment(Pos.CENTER);
		previewBlendLabel.setMaxHeight(Double.MAX_VALUE);
		TextField previewBlendOpacityField = new TextField("100 %");
		previewBlendOpacityField.setMaxWidth(80);
		ComboBox<BlendMode> previewBlendModeSelector = new ComboBox<BlendMode>(FXCollections.observableArrayList(BlendMode.ADD, BlendMode.MULTIPLY, BlendMode.OVERLAY));
		previewBlendModeSelector.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(previewBlendModeSelector, Priority.ALWAYS);
		HBox previewBlendPane = new HBox(previewBlendLabel, previewBlendOpacityField, previewBlendModeSelector);
		previewBlendPane.setPadding(new Insets(4));
		previewBlendPane.setSpacing(4);
		setTop(previewBlendPane);
		
		//Setup layer list pane
		ScrollPane layerListScrollPane = new ScrollPane(layersListVBox);
		layerListScrollPane.setFocusTraversable(false);
		layerListScrollPane.setVbarPolicy(ScrollBarPolicy.ALWAYS);
		layerListScrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
		
		
		for(LayerWrapper<?> layerWrapper: compositionWrapper.getObservableLayersList()) {
			browserAddLayer(layerWrapper);
		}
		
		compositionWrapper.getObservableLayersList().addListener((ListChangeListener.Change<? extends LayerWrapper<?>> c) -> {
			while(c.next()) {
				for(LayerWrapper<?> l: c.getRemoved()) {
					browserRemoveLayer(l);
				}
				for(LayerWrapper<?> l: c.getAddedSubList()) {
					browserAddLayer(l);
				}
			}
		});
		
		compositionWrapper.selectedLayerProperty().addListener((observable, oldValue, newValue) -> {
			if(oldValue != null) {
				LayerView oldLayerView = layersMap.get(oldValue);
				oldLayerView.setSelected(false);
				previewBlendModeSelector.valueProperty().unbindBidirectional(oldLayerView.getLayerWrapper().layerPreviewBlendmodeProperty());
			}
			if(newValue != null) {
				LayerView newLayerView = layersMap.get(newValue);
				newLayerView.setSelected(true);
				previewBlendModeSelector.valueProperty().bindBidirectional(newLayerView.getLayerWrapper().layerPreviewBlendmodeProperty());
			}
		});
		
		setCenter(layerListScrollPane);
	}
	
	void selectLayerByView(LayerView layerView) {
		compositionWrapper.setSelectedLayer(layerView.getLayerWrapper());
	}
	
	private LayerView browserAddLayer(LayerWrapper<?> layer) {
		LayerView view = new LayerView(layer, this);
		layersMap.put(layer, view);
		layersListVBox.getChildren().add(view);
		return view;
	}
	
	private void browserRemoveLayer(LayerWrapper<?> layer) {
		LayerView view = layersMap.remove(layer);
		layersListVBox.getChildren().remove(view);
	}
}
