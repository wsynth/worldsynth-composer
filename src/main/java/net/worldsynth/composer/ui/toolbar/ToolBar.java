/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.composer.ui.toolbar;

import java.util.ArrayList;

import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import net.worldsynth.composer.layer.LayerWrapper;
import net.worldsynth.composer.tool.Tool;
import net.worldsynth.composer.tool.ToolCursor;
import net.worldsynth.composer.tool.ToolRegistry;
import net.worldsynth.composer.ui.WorldSynthComposerEditor;

public class ToolBar extends javafx.scene.control.ToolBar {
	
	Tool<?> selectedTool = new ToolCursor();
	
	public ToolBar() {
		setOrientation(Orientation.VERTICAL);
	}
	
	public void setToolsForLayer(LayerWrapper<?> layer) {
		setTools(ToolRegistry.getToolsForLayer(layer));
	}
	
	public void setTools(ArrayList<Tool<?>> tools) {
		getItems().clear();
		for(Tool<?> t: tools) {
			Button toolButton = new Button();
			toolButton.setGraphic(new ImageView(t.getToolIconImage()));
			toolButton.setOnAction(e -> {
				selectedTool = t;
				WorldSynthComposerEditor.toolParametersPaneWrapper.setCenter(t.getToolParameterPane());
			});
			getItems().add(toolButton);
		}
		selectedTool = tools.get(0);
	}
	
	public Tool<?> getSelectedTool() {
		return selectedTool;
	}
}
